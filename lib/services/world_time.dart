import 'package:http/http.dart';
import 'dart:convert';
import 'package:intl/intl.dart';

class WorldTime {
  String location; //Location name for UI
  String time; //Time in that location
  String url; //Location url for api endpoint
  List<dynamic> listCity;
  bool isDaytime;

  WorldTime({this.location, this.url});

Future<void> getTimezone() async {
    try {
      //Make Request
      String endpoint = "http://worldtimeapi.org/api/timezone";
      Response response = await get(endpoint);
      List<dynamic> data = jsonDecode(response.body);
      //get properties from data
      listCity = data; 
      // print(datetime);
      //print(offset)
      } catch (e) {
      print("Error: $e");
      time = "Could not get Time Date";
    }
  }
  Future<void> getTime() async {
    try {
      //Make Request
      String endpoint = "http://worldtimeapi.org/api/timezone/$url";
      Response response = await get(endpoint);
      Map data = jsonDecode(response.body);
      //get properties from data

      String datetime = data['utc_datetime'];
      String offset = data['utc_offset'].substring(1, 3);
      // print(datetime);
      //print(offset);

      //Create DateTime object
      DateTime now = DateTime.parse(datetime);
      now = now.add(Duration(hours: int.parse(offset)));

      //Set Time Property
      isDaytime = now.hour > 6 && now.hour <20 ? true : false;
      time = DateFormat.jm().format(now); 
    } catch (e) {
      print("Error: $e");
      time = "Could not get Time Date";
    }
  }
}
