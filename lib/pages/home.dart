import 'dart:ui';

import 'package:flutter/material.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {

  Map data = {};

  @override
  Widget build(BuildContext context) {

    data = data.isNotEmpty ? data : ModalRoute.of(context).settings.arguments;
    print(data); 
 var day = LinearGradient(
    begin: Alignment.bottomCenter,
    end: Alignment.topCenter,
    colors: [
      Colors.white,
      Colors.blue[300],
      ],
     );
  var night = LinearGradient(
    begin: Alignment.bottomCenter,
    end: Alignment.topCenter,
    colors: [
      Colors.blue[400],
      Colors.blue[900],
      ],
  );

  LinearGradient gradientSelected = data["isDaytime"] ? day : night;
  Color fontColor = data["isDaytime"] ? Colors.black : Colors.white;

    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
               gradient: gradientSelected,
            ), 
        child: SafeArea( 
          child: Padding(
            padding: const EdgeInsets.fromLTRB(0, 200.0, 0, 0),
            child: Column(
              children: <Widget>[
                  FlatButton.icon(
                      onPressed: () async {
                      dynamic result = await Navigator.pushNamed(context, "/location");
                      setState(() {
                        data = {
                          "time": result["time"],
                          "location": result["location"],
                          "isDaytime": result["isDaytime"],
                        };
                      });
                      },
                    icon: Icon(
                      Icons.edit_location,
                      color: fontColor,
                      ),
                    label: Text(
                                "Edit Location",
                                style: TextStyle(color: fontColor,
                                ),
                               )
                  ),
                  SizedBox(height: 20.0),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        data["location"],
                        style: TextStyle(
                          fontSize: 28.0,
                          letterSpacing: 2.0,
                          color: fontColor,
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 20.0),
                  Text(
                    data["time"],
                    style: TextStyle(
                      fontSize: 66.0,
                      color: fontColor
                    ),
                  ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
