import 'package:flutter/material.dart';
import 'package:world_time/services/world_time.dart';
import "package:flutter_spinkit/flutter_spinkit.dart";

class Loading extends StatefulWidget {
  @override
  _LoadingState createState() => _LoadingState();
}

class _LoadingState extends State<Loading> {

  String time = "Loading";

  void setupWorldTime() async {
    WorldTime instance = WorldTime(location: "Rome", url: "Europe/Rome");
    await instance.getTimezone();
    await instance.getTime();
    Navigator.pushReplacementNamed(context, "/home", arguments: {
      "location": instance.location,
      "time": instance.time,
      "isDaytime": instance.isDaytime,
      "listCity": instance.listCity,
    });
    setState(() {
      time = instance.time;
    });
  }

  @override
  void initState() {
    super.initState();
   setupWorldTime();
    
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blue[900],
      body: Center(
        child: SpinKitCircle(
          color: Colors.white,
          size: 50.0,
          ),

      ),
    );
  }
}